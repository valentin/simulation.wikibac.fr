function update() {
    let controle_continu = calcul_section([
        { code: 'EMC', entries: ['EMC-1re', 'EMC-tle'], coef: 1 },
        { code: 'LVA', entries: ['LVA-1re', 'LVA-tle'], coef: 3 },
        { code: 'LVB', entries: ['LVB-1re', 'LVB-tle'], coef: 3 },
        { code: 'MAT', entries: ['MAT-1re', 'MAT-tle'], coef: 3 },
        { code: 'HG', entries: ['HG-1re', 'HG-tle'], coef: 3 },
        { code: 'EPS', entries: ['EPS'], coef: 6 },
        { code: 'SP3', entries: ['SP3'], coef: 8 },
    ]);

    let epreuves_terminales = calcul_section([
        { code: 'FR-ecrit', entries: ["FR-ecrit"], coef: 5 },
        { code: 'FR-oral', entries: ["FR-oral"], coef: 5 },
        { code: 'PHILO', entries: ["PHILO"], coef: 4 },
        { code: 'GO', entries: ["GO"], coef: 14 },
        { code: 'SP1', entries: ["SP1"], coef: 16 },
        { code: 'SP2', entries: ["SP2"], coef: 16 },
    ]);

    let options = calcul_section([
        { code: 'OPT1', entries: ["OPT1-1re", "OPT1-tle"], coef: 2 },
        { code: 'OPT2', entries: ["OPT2-1re", "OPT2-tle"], coef: 2 },
    ])

    let points = controle_continu.points + epreuves_terminales.points + options.points;
    let coefficients = controle_continu.coefficients + epreuves_terminales.coefficients + options.coefficients;

    decider(points, coefficients);
}