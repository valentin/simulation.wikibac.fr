function update() {
    let controle_continu = calcul_section([
        { code: 'EMC', entries: ['EMC-1re', 'EMC-tle'], coef: 1 },
        { code: 'LVB', entries: ['LVB-1re', 'LVB-tle'], coef: 3 },
        { code: 'ES', entries: ['ES-1re', 'ES-tle'], coef: 3 },
        { code: 'EPS', entries: ['EPS'], coef: 6 },
        { code: 'SP3', entries: ['SP3'], coef: 8 },
    ]);

    let epreuves_terminales = calcul_section([
        { code: 'FR-ecrit', entries: ["FR-ecrit"], coef: 5 },
        { code: 'FR-oral', entries: ["FR-oral"], coef: 5 },
        { code: 'PHILO', entries: ["PHILO"], coef: 8 },
        { code: 'GO', entries: ["GO"], coef: 10 },
        { code: 'SP1', entries: ["SP1"], coef: 16 },
        { code: 'SP2', entries: ["SP2"], coef: 16 },
    ]);

    let evaluations_specifiques = calcul_section([
        { code: 'ACL-ecrit', entries: ["ACL-ecrit"], coef: 10 },
        { code: 'ACL-oral', entries: ["ACL-oral"], coef: 10 },
        { code: 'CDM', entries: ["CDM"], coef: 20 },
        { code: 'DNL1-ecrit', entries: ["DNL1-ecrit"], coef: 10 },
        { code: 'DNL1-oral', entries: ["DNL1-oral"], coef: 10 },
    ]);

    let options = calcul_section([
        { code: 'LCA', entries: ["LCA-1re", "LCA-tle"], coef: 2 },
        { code: 'OPT1', entries: ["OPT1-1re", "OPT1-tle"], coef: 2 },
        { code: 'OPT2', entries: ["OPT2"], coef: 2 },
        { code: 'DNL2', entries: ["DNL2"], coef: 20 },
    ])

    let points = controle_continu.points + epreuves_terminales.points + evaluations_specifiques.points + options.points;
    let coefficients = controle_continu.coefficients + epreuves_terminales.coefficients + evaluations_specifiques.coefficients + options.coefficients;

    decider(points, coefficients);
}