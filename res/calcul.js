function arrondi_dixieme(nombre) {
    return Math.ceil(10*nombre) / 10;
}

function arrondi_centieme(nombre) {
    return Math.ceil(100*nombre) / 100;
}

function representer(nombre) {
    return (isNaN(nombre)) ? '&ndash;' : nombre.toLocaleString();
}

function calcul_section(data) {
    /* data est de la forme array[object] avec object de la forme
    { 'code': str,
      'coef': int,
      'entries': array }

    Sortie : object de la forme { 'points': float, 'coefficients': int }
    */

    let points= 0, coefficients= 0;

    data.forEach(epreuve => {
        let points_epreuve= 0;

        let caduque = true;
        epreuve.entries.forEach(entry => {
            let field = document.getElementById(entry + "-note");

            if (field.value && field.checkValidity()) {
                points_epreuve += epreuve.coef * arrondi_dixieme(parseFloat(field.value));
                coefficients += epreuve.coef;
                caduque = false;
            } else if (field.required) {
                coefficients += epreuve.coef;
            }
        });

        document.getElementById(epreuve.code + '-points').innerHTML = representer((caduque) ? NaN : points_epreuve);
        points += points_epreuve;
    })

    return {"points": points, "coefficients": coefficients};
}

function decider(points, coefficients) {
    let valide = document.getElementById("simulateur").checkValidity();
    document.getElementById("total-points").innerHTML = representer((valide) ? points : NaN);
    document.getElementById("total-coef").innerHTML = representer(coefficients);
    let note_brute = (valide) ? points/coefficients : NaN;
    let note_finale = arrondi_centieme(note_brute);
    document.getElementById("note-finale").innerHTML = representer(note_finale);

    let decision;
    let sup_mention_actuelle;
    if (note_finale < 8) {
        decision = "Ajourné"
        sup_mention_actuelle = 7.99*coefficients
    } else if (note_finale < 10) {
        decision = "Passe le second groupe";
        sup_mention_actuelle = 9.99*coefficients
    } else if (note_finale < 12) {
        decision = "Admis";
        sup_mention_actuelle = 11.99*coefficients
    } else if (note_finale < 14) {
        decision = "Admis Mention Assez Bien";
        sup_mention_actuelle = 13.99*coefficients
    } else if (note_finale < 16) {
        decision = "Admis Mention Bien";
        sup_mention_actuelle = 15.99*coefficients
    } else if (note_finale < 18) {
        decision = "Admis Mention Très Bien";
        sup_mention_actuelle = 17.99*coefficients
    } else if (note_finale >= 18) {
        decision = "Admis Mention Très Bien avec les félicitations du jury";
        sup_mention_actuelle = NaN
    } else {
        decision = "&ndash;"
    }

    if (note_finale < 18) {
        let points_mention_suivante = arrondi_dixieme(sup_mention_actuelle - points + .01)
        document.getElementById("points-manquants").innerHTML = representer(points_mention_suivante);
    } else {
        document.getElementById("points-manquants").innerHTML = "&ndash;";
    }


    document.getElementById("decision-jury").innerHTML = decision;
}